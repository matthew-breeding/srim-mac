# SRIM for MacOS
======================================

To install, clone the git repository (`git clone git@gitlab.com:matthew-breeding/srim-mac.git`)
or download compressed files in whatever format you prefer and unpack. Then run:

`chmod +x configure` 

`./configure`

then follow prompts for installing as they pop up. After this, you should be able to run SRIM with `srim` command.  

Requires VirtualBox and Vagrant (install script will prompt the user
on how to install these programs if not already on the machine)

Tested on MacOS Big Sur 11.1 -- contact Matthew Breeding
<matthew.l.breeding@vanderbilt.edu> with bugs/feedback etc.
